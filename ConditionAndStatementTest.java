/*
    This class is used to check the statement and the condition both. Please go through the code and reply in the 
    comment below what you have ubnderstood.
*/



    import java.util.Scanner;
    Class ConditionAndStatementTest{
	public static void main(String ...args) {
        
		Scanner out  = new Scanner(System.in);
		System.out.println("Enter the Number 1:-");
		int num1 = out.nextInt();
		System.out.println("Enter the Number 2:-");
		int num2 = out.nextInt();
		System.out.println("Enter any one of  the Symbol:  ");
		System.out.println("For Adddition press 1");
		System.out.println("For Subtraction press 2");
		System.out.println("For Multiplication press 3");
		System.out.println("For Division press 4");
		int symbol = out.nextInt();
		
		new Test().ConditionAndStatement(num1, num2, symbol);
    }
    public void ConditionAndStatement(int number1, int number2,int symbol ){
			int totalValue=0;
			if(number1>=10 && number2<=100){
				System.out.println(" You have Entered the numbers as: "+number1+","+number2);
				switch (symbol) {
				case 1:
					totalValue = number1+number2;
					System.out.println("You have Selected 1 - Total Sum is:-> "+totalValue);
					break;
				case 2:
					totalValue = number1-number2;
					System.out.println("You have Selected 2 - Total Sum is:-> "+totalValue);
					break;
				case 3:
					totalValue = number1*number2;
					System.out.println("You have Selected 3 - Total Sum is:-> "+totalValue);
					break;
				case 4:
					totalValue = number1/number2;
					System.out.println("You have Selected 4 - Total Sum is:-> "+totalValue);
					break;	
				default:
					System.out.println("Sorry!!! You have passed wrong symbol:: We can't do any calculation ");
					break;
				}
			}else{
				System.out.println("Kindly pass the number1 above 10 and number 2 below  100 -  Here the Functionality of Condition is understood!!!");
			}
	}